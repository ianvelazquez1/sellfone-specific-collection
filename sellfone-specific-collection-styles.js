import { css } from 'lit-element';

export default css`
:host {
  display: inline-block;
  box-sizing: border-box;
  width: 100%; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit;
  font-family: inherit; }

.mainContainer {
  display: grid;
  grid-template-columns: 20% 80%; }

.filtersContainer {
  background-color: #00305e;
  color: white;
  border-radius: 5px;
  border: 1px solid #666666;
  display: flex;
  height: 500px;
  flex-flow: row wrap; }
  .filtersContainer .title {
    padding: 5px 10px; }
  .filtersContainer .filterContainer {
    width: 100%;
    padding: 0px 10px; }
    .filtersContainer .filterContainer .filterTitleCont {
      border-bottom: 1px solid gray;
      padding-bottom: 10px; }
      .filtersContainer .filterContainer .filterTitleCont .filterTitle {
        color: white;
        padding: 10px 0;
        margin: 0;
        font-size: 1.1rem;
        font-weight: bold; }
      .filtersContainer .filterContainer .filterTitleCont .filterText {
        text-decoration: none;
        color: rgba(255, 255, 255, 0.616); }

.productsContainer .collectionTitleContainer {
  margin: 20px 0;
  width: 100%;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  padding: 0 20px; }

.productsContainer .productGrid {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  column-gap: 10px;
  justify-items: center; }

.productsContainer .product {
  margin: 20px 0px;
  --product-component-max-width:200px;
  --height-width:none;
  --product-component-min-width:200px; }
`;
