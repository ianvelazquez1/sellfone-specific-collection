/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-specific-collection.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-specific-collection></sellfone-specific-collection>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
