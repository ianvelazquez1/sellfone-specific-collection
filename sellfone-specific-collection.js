import { html, LitElement } from 'lit-element';
import style from './sellfone-specific-collection-styles.js';
import '@catsys/sellfone-featured-products/sellfone-featured-products.js';
import '@catsys/product-component/product-component.js';

class SellfoneSpecificCollection extends LitElement {
  static get properties() {
    return {
      filtersArray:{
        type:Array,
        attribute:'filters-array'
      },
      featuredOptions:{
        type:Object,
        attribte:'featured-options'
      },
      collectionOptions:{
        type:Object,
        attribute:'collection-options'
      },
      productArray:{
        type:Object,
        attribute:'product-array'
      }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.filtersArray=[
      {filterName:'Marca',filters:[{name:'Galaxy S7',propertyToFilter:'name',value:'Galaxy'}]},
      {filterName:'Modelo',filters:[{name:'Galaxy S7',propertyToFilter:'name',value:'Galaxy'}]},
      {filterName:'Condición',filters:[{name:'Galaxy S7',propertyToFilter:'name',value:'Galaxy'}]}
    ]
    this.featuredOptions={
      imageSrc:"https://imagens.trocafone.com/images/home_hightlights_banners/20190128125422-Brazil-Highlight_Samsung_Desktop.jpg",
      imageUrl:"#1",
      product1:{id: 'S23R2',
     title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
     detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
     images: [
         'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
         'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
         'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
         'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
         'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
     ],
     price: 2999,
     discountedPrice: 2350},
      product2:{id: 'S23R2',
     title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
     detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
     images: [
         'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
     ],
     price: 2999,
     discountedPrice: 0,}
    }
    this.collectionOptions={
      collectionTitle:'Collection title',
    }
    this.productArray=[{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/xr/iphone-xr-white-select-201809?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1551226036668'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/xr/iphone-xr-white-select-201809?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1551226036668'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/xr/iphone-xr-white-select-201809?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1551226036668'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/xr/iphone-xr-white-select-201809?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1551226036668'
      ],
      price: 2999,
      discountedPrice: 0
  },{
      id: 'S23R2',
      title: 'Celular Samsung Galaxy S7 Flat (seminuevo)',
      detail: 'No se trata de un nuevo smartphone, sino de un nuevo concepto sobre telefonía. Redescubre lo que un teléfono puede hacer con Celular Samsung Galaxy S7 Flat',
      images: [
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/frontal.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/perspectiva.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/lateral.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/trasera.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg',
          'https://www.telcel.com/content/dam/telcelcom/dispositivos/Samsung/SM-G975F/imagenes/azul/dual.jpg/jcr:content/renditions/cq5dam.web.250.470.jpeg'
      ],
      price: 2999,
      discountedPrice: 0
  }]
  }

  render() {
    return html`
    <div class="mainContainer">
        <div class="filtersContainer">
         <p class="title">Filtrado por</p>
           ${this.filtersArray.map(filter=>{
             return html`
              <div class="filterContainer">
                <div class="filterTitleCont">
                    <p class="filterTitle">
                        ${filter.filterName}
                    </p>
                  
                    ${filter.filters.map(mainFilters=>{
                        return html`<a class="filterText" href="#">${mainFilters.name}</a>`
                      })}
                 
                </div>
              <div>
             `;
           })}     
          </div>
        <div class="productsContainer">
           <sellfone-featured-products .options="${this.featuredOptions}"></sellfone-featured-products>
           <div class="collectionTitleContainer">
             <h3 class="titleCollection">${this.collectionOptions.collectionTitle}</h3>
             <select>
               <option>Mayor-menor precio</option>
               <option></option>
               <option></option>
               <option></option>
               <option></option>
             </select>
           </div>
           <div class="productGrid">
             ${this.productArray.map(product=>{
               return html`<product-component .objectCell="${product}" class="product"></product-component>`
             })}
          
           </div>
        </div>
    </div>
      `;
    }

    firstUpdated(){

    }
}

window.customElements.define("sellfone-specific-collection", SellfoneSpecificCollection);
